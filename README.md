# React basics

## Теоретический блок по основам React

### Рекомендации

Рекомендуется внимательно изучить материалы из данного блока. Материалы из данного блока будут спрашивать на собеседованиях в 90% случаев

## Материалы для изучения React
- [Полный курс по React](https://www.youtube.com/watch?v=GNrdg3PzpJQ&list=PL6DxKON1uLOHya4bDIynPTCwZHrezUlFs) - рекомендуется тем у кого пока мало опыта работы с данной библиотекой
- [Документация по React](https://react.dev/learn) - новая версия документации, не путать со старой!
- [Как рабоает virtual DOM и React](https://www.youtube.com/watch?v=A0W2n2azH5s) - лучший разбор как работоает механизм пересогласования в React
- [Жизненный цикл компонента React](https://www.youtube.com/watch?v=O8f6aXqpGHw) - хоть классовые компоненты это легаси, полезно знать на их примере жизненный цикл компонента и знать какие из хуков какому методу соответствуют и когда отрабатываю
- [Плейлист с разбором хуков функциональных компонентов](https://www.youtube.com/watch?v=A0W2n2azH5s&list=PLz_dGYmQRrr-g02jHDzuu-6VlOt8-8Uu5) - настоятельно рекомендуется посмотреть как работают хуки useEffect, useLayoutEffect, useRef, useMemo и useCallback
- [Плейлист по роутингу](https://www.youtube.com/watch?v=0auS9DNTmzE&list=PLiZoB8JBsdznY1XwBcBhHL9L7S_shPGVE) - подробный разбор актуальной версии React router dom

## Материалы по ФП которые следует знать для работы с React

- [Функциональное программирование](https://www.youtube.com/watch?v=ScgmlDb5ed4&t=7s) - основы ФП, смотреть до темы Композиция
- [Замыкания](https://www.youtube.com/watch?v=2zQapitrXSY) - самое простое и понятное объяснение замыкания "на пальцах"